lightbox.option({
  alwaysShowNavOnTouchDevices: true,
  imageFadeDuration: 0,
  resizeDuration: 200,
  wrapAround: true
})

$(function() {
  $('.fiction a').click(function(){
    gtag('event', 'open_link', {
      event_category: 'Story',
      event_label: $(this).text()
    });
  });
  $('.coding a, .header-content a, .footer a').click(function(){
    var $this = $(this);
    if($this.data('lightbox')){
      gtag('event', 'open_link', {
        event_category: 'Lightbox',
        event_label: $this.data('lightbox')
      });
    } else {
      gtag('event', 'open_link', {
        event_category: 'Link',
        event_label: $this.attr('href')
      });
    }
  });
  // TODO: Add JqueryUI draggable, and do the math (https://codepen.io/graphilla/pen/MybXwy) to programmatically update rotateX/rotateY on .home-box
});