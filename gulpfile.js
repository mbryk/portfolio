const gulp        = require('gulp'),
      sass        = require('gulp-sass'),
      connect     = require('gulp-connect'),
      htmlmin     = require('gulp-htmlmin'),
      babel       = require('gulp-babel'),
      // concat      = require('gulp-concat'),
      uglify      = require('gulp-uglify'),
      rename      = require('gulp-rename'),
      cssmin      = require('gulp-cssnano'),
      prefix      = require('gulp-autoprefixer'),
      sourcemaps  = require('gulp-sourcemaps'),
      open        = require('gulp-open');

const startServer = cb => {
  connect.server({
    root: 'public',
    livereload: true
  });
  cb();
}

const livereload = () => {
  return gulp.src('./public/**/*').pipe(connect.reload());
}

const html = () => {
  return gulp.src('./src/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true, removeComments: true }))
    .pipe(gulp.dest('./public'));
}

const scripts = () => {
  return gulp.src('./src/js/main.js')
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js'))
}

const styles = () => {
  return gulp.src('./src/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({ errLogToConsole: true, outputStyle: 'expanded' }))
    .pipe(prefix())
    .pipe(rename('main.css'))
    .pipe(gulp.dest('./public/css'))
    .pipe(cssmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./public/css'))
}

const build = cb => {
  return gulp.parallel(html, scripts, styles)(cb);
}

const watch = cb => {
  gulp.watch('./src/**/*.html', gulp.series(html, livereload));
  gulp.watch('./src/js/**/*.js', gulp.series(scripts, livereload));
  gulp.watch('./src/scss/**/*.scss', gulp.series(styles, livereload));
  cb();
}

const watchWithoutReload = cb => {
  gulp.watch('./src/**/*.html', html);
  gulp.watch('./src/js/**/*.js', scripts);
  gulp.watch('./src/scss/**/*.scss', styles);
  cb();
}

const browser = () => {
  return gulp.src(__filename)
    .pipe(open({
      uri: 'http://localhost:8080'
    }))
}

module.exports = {
  build: build,
  firebase: gulp.series(watchWithoutReload, build),
  default: gulp.series(startServer, watch, build, browser)
}