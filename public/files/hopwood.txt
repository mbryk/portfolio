// To learn how to use this script, refer to the documentation:
// https://developers.google.com/apps-script/samples/automations/mail-merge

/*
Copyright 2022 Martin Hawksey

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
 
/**
 * @OnlyCurrentDoc
*/
 
/**
 * Change these to match the column names you are using for email 
 * recipient addresses and email sent column.
*/
const RECIPIENT_COL  = "Email";
const EMAIL_SENT_COL = "Email Sent";
 
/** 
 * Creates the menu item "Mail Merge" for user to run scripts on drop-down.
 */
function onOpen() {
  const ui = SpreadsheetApp.getUi();
  ui.createMenu('Mail Merge')
      .addItem('Send Winner Emails', 'sendWinnerEmails')
      .addItem('Send Rejection Emails', 'sendRejectionEmails')
      .addToUi();
}
 
/**
 * Sends emails from sheet data.
 * @param {Sheet} sheet to read data from
*/
function sendWinnerEmails(sheet=SpreadsheetApp.getActiveSheet()) {
  const ui = SpreadsheetApp.getUi();
  const response = ui.prompt("How many emails to send in this batch?", ui.ButtonSet.OK)
  const limit = parseInt(response.getResponseText())
  if (Number.isNaN(limit)) {
    ui.alert("That is not a valid number")
    return
  }
  
  // Gets the draft Gmail message that starts with "Good news!" to use as a template
  const emailTemplate = getGmailTemplateFromDrafts_("Good news!");
  
  // Gets the data from the passed sheet
  const dataRange = sheet.getDataRange();
  // Fetches displayed values for each row in the Range HT Andrew Roberts 
  // https://mashe.hawksey.info/2020/04/a-bulk-email-mail-merge-with-gmail-and-google-sheets-solution-evolution-using-v8/#comment-187490
  // @see https://developers.google.com/apps-script/reference/spreadsheet/range#getdisplayvalues
  const data = dataRange.getDisplayValues();

  // Assumes row 1 contains our column headings
  const heads = data.shift(); 
  
  // Gets the index of the column named 'Email Status' (Assumes header names are unique)
  // @see http://ramblings.mcpher.com/Home/excelquirks/gooscript/arrayfunctions
  const emailSentColIdx = heads.indexOf(EMAIL_SENT_COL);
  
  // Converts 2d array into an object array
  // See https://stackoverflow.com/a/22917499/1027723
  // Also https://mashe.hawksey.info/?p=17869/#comment-184945
  const obj = data.map(function(values) {
    return heads.reduce(function(o, k, i) {
      o[k.trim()] = values[i];
      return o;
    }, {});
  });

  // Creates an array to record sent emails
  const out = [];

  let amtDone = 0
  // Loops through all the rows of data
  obj.every(function(row, rowIdx){
    // Only sends emails if email_sent cell is blank and not hidden by a filter
    if (row[EMAIL_SENT_COL] == ''){
      try {
        const msgObj = fillInTemplateFromObject_(emailTemplate.message, row, true);

        // See https://developers.google.com/apps-script/reference/gmail/gmail-app#sendEmail(String,String,String,Object)
        // If you need to send emails with unicode/emoji characters change GmailApp for MailApp
        // Uncomment advanced parameters as needed (see docs for limitations)
        GmailApp.sendEmail(row[RECIPIENT_COL], msgObj.subject, msgObj.text, {
          htmlBody: msgObj.html,
          // bcc: 'a.bcc@email.com',
          // cc: 'a.cc@email.com',
          from: 'hopwoodassistants@umich.edu',
          name: 'Hopwood Awards',
          // replyTo: 'a.reply@email.com',
          // noReply: true, // if the email should be sent from a generic no-reply email address (not available to gmail.com users)
          attachments: emailTemplate.attachments,
          inlineImages: emailTemplate.inlineImages
        });
        
        // Edits cell to record email sent date
        out.push([new Date()]);
      } catch(e) {
        // modify cell to record error
        out.push(["Error: " + e.message]);
      }
      // increment even if it didn't send?
      amtDone++;
    } else {
      out.push([row[EMAIL_SENT_COL]]);
    }
    if (amtDone >= limit) {
      return false; // break
    }
    return true;
  });
  
  // Updates the sheet with new data
  sheet.getRange(2, emailSentColIdx+1, out.length).setValues(out);
  
}

function sendRejectionEmails(sheet=SpreadsheetApp.getActiveSheet()) {
  const ui = SpreadsheetApp.getUi();
  const response = ui.prompt("How many emails to send in this batch?", ui.ButtonSet.OK)
  const limit = parseInt(response.getResponseText())
  if (Number.isNaN(limit)) {
    ui.alert("That is not a valid number")
    return
  }
  
  // Gets the draft Gmail message that starts with this text to use as a template
  const emailTemplate = getGmailTemplateFromDrafts_("Sorry, you have");
  const dataRange = sheet.getDataRange();
  const data = dataRange.getDisplayValues();
  const heads = data.shift();
  const emailSentColIdx = heads.indexOf(EMAIL_SENT_COL);
  const obj = data.map(function(values) {
    return heads.reduce(function(o, k, i) {
      o[k.trim()] = values[i];
      return o;
    }, {});
  });
  const out = [];

  let amtDone = 0
  // Loops through all the rows of data
  obj.every(function(row, rowIdx){
    // Only sends emails if email_sent cell is blank and not hidden by a filter
    if (row[EMAIL_SENT_COL] == ''){
      try {
        const msgObj = fillInTemplateFromObject_(emailTemplate.message, row, false);

        // See https://developers.google.com/apps-script/reference/gmail/gmail-app#sendEmail(String,String,String,Object)
        // If you need to send emails with unicode/emoji characters change GmailApp for MailApp
        // Uncomment advanced parameters as needed (see docs for limitations)
        GmailApp.sendEmail(row[RECIPIENT_COL], msgObj.subject, msgObj.text, {
          htmlBody: msgObj.html,
          // bcc: 'a.bcc@email.com',
          // cc: 'a.cc@email.com',
          from: 'hopwoodassistants@umich.edu',
          name: 'Hopwood Awards',
          // replyTo: 'a.reply@email.com',
          // noReply: true, // if the email should be sent from a generic no-reply email address (not available to gmail.com users)
          attachments: emailTemplate.attachments,
          inlineImages: emailTemplate.inlineImages
        });
        
        // Edits cell to record email sent date
        out.push([new Date()]);
      } catch(e) {
        // modify cell to record error
        out.push(["Error: " + e.message]);
      }
      // increment even if it didn't send?
      amtDone++;
    } else {
      out.push([row[EMAIL_SENT_COL]]);
    }
    if (amtDone >= limit) {
      return false; // break
    }
    return true;
  });
  
  // Updates the sheet with new data
  sheet.getRange(2, emailSentColIdx+1, out.length).setValues(out);
}

/**
 * Get a Gmail draft message by matching the subject line.
 * @param {string} subject_line to search for draft message
 * @return {object} containing the subject, plain and html message body and attachments
*/
function getGmailTemplateFromDrafts_(subject_line){
  try {
    // get drafts
    const drafts = GmailApp.getDrafts();
    // filter the drafts that match subject line
    const draft = drafts.filter(subjectFilter_(subject_line))[0];
    // get the message object
    const msg = draft.getMessage();

    // Handles inline images and attachments so they can be included in the merge
    // Based on https://stackoverflow.com/a/65813881/1027723
    // Gets all attachments and inline image attachments
    const allInlineImages = draft.getMessage().getAttachments({includeInlineImages: true,includeAttachments:false});
    const attachments = draft.getMessage().getAttachments({includeInlineImages: false});
    const htmlBody = msg.getBody(); 

    // Creates an inline image object with the image name as key 
    // (can't rely on image index as array based on insert order)
    const img_obj = allInlineImages.reduce((obj, i) => (obj[i.getName()] = i, obj) ,{});

    //Regexp searches for all img string positions with cid
    const imgexp = RegExp('<img.*?src="cid:(.*?)".*?alt="(.*?)"[^\>]+>', 'g');
    const matches = [...htmlBody.matchAll(imgexp)];

    //Initiates the allInlineImages object
    const inlineImagesObj = {};
    // built an inlineImagesObj from inline image matches
    matches.forEach(match => inlineImagesObj[match[1]] = img_obj[match[2]]);

    return {
      message: {
        subject: msg.getSubject(),
        text: msg.getPlainBody(),
        html: htmlBody
      }, 
      attachments: attachments,
      inlineImages: inlineImagesObj
    };
  } catch(e) {
    throw new Error("Oops - can't find Gmail draft");
  }

  /**
   * Filter draft objects with the matching subject linemessage by matching the subject line.
   * @param {string} subject_line to search for draft message
   * @return {object} GmailDraft object
  */
  function subjectFilter_(subject_line){
    return function(element) {
      if (element.getMessage().getSubject().startsWith(subject_line)) {
        return element;
      }
    }
  }
}

/**
 * Fill template string with data object
 * @see https://stackoverflow.com/a/378000/1027723
 * @param {string} template string containing {{}} markers which are replaced with data
 * @param {object} data object used to replace {{}} markers
 * @return {object} message replaced with data
*/
function fillInTemplateFromObject_(template, data, isWinner = false) {
  // We have two templates one for plain text and the html body
  // Stringifying the object means we can do a global replace
  let template_string = JSON.stringify(template);
  
  // Browser.msgBox("filling in template:  " + JSON.stringify(template) + " ///////// " + JSON.stringify(data))
  if (isWinner) {
    if (!data["Place 1"]) {
      throw new Error("No award listed")
    }
    // AWARDS REPLACEMENT
    const awards = []

    if (data["Place 1"]) {
      awards.push(`${data['Place 1']} ${data['T1']} ${data['Award 1']} ${data['T2']} ${data['Title 1']} ${data['T3']} ${data['Amount 1']}`)
    }
    if (data["Place 2"]) {
      awards.push(`${data['Place 2']} ${data['T4']} ${data['Award 2']} ${data['T5']} ${data['Title 2']} ${data['T6']} ${data['Amount 2']}`)
    }
    if (data["Place 3"]) {
      awards.push(`${data['Place 3']} ${data['T7']} ${data['Award 3']} ${data['T8']} ${data['Title 3']} ${data['T9']} ${data['Amount 3']}`)
    }
    if (data["Place 4"]) {
      awards.push(`${data['Place 4']} ${data['T10']} ${data['Award 4']} ${data['T11']} ${data['Title 4']} ${data['T12']} ${data['Amount 4']}`)
    }
    if (data['Ao award 1']) {
      let string = `${data['Ao intro']} ${data['Ao award 1']} ${data['Ao t1']} ${data['Ao amount 1']} ${data['Ao t2']} ${data['Ao title']}`
      if (data['Ao award 2']) {
        string += ` ${data['Ao t3']} ${data['Ao award 2']} ${data['Ao t4']} ${data['Ao amount 2']} ${data['Ao t5']} ${data['Ao title 2']}`
      }
    }

    data['Awards'] = awards.join("<br />");
  }
 
  // Token replacement
  template_string = template_string.replace(/{{[^{}]+}}/g, key => {
    return escapeData_(data[key.replace(/[{}]+/g, "")] || "");
  });

  return JSON.parse(template_string);
}

/**
 * Escape cell data to make JSON safe
 * @see https://stackoverflow.com/a/9204218/1027723
 * @param {string} str to escape JSON special characters from
 * @return {string} escaped string
*/
function escapeData_(str) {
  return str
    .replace(/[\\]/g, '\\\\')
    .replace(/[\"]/g, '\\\"')
    .replace(/[\/]/g, '\\/')
    .replace(/[\b]/g, '\\b')
    .replace(/[\f]/g, '\\f')
    .replace(/[\n]/g, '\\n')
    .replace(/[\r]/g, '\\r')
    .replace(/[\t]/g, '\\t');
};
